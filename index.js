//import thu vien
const express=require('express');
const path=require('path');
const bodyParser=require('body-parser');

const mongoose=require('mongoose');


const vaccinationRouter=require('./app/router/vaccinationRouter');
const userRouter=require('./app/router/userRouter');
const contactRouter=require('./app/router/contactRouter');
const adminUserRouter=require('./app/router/admin-userRouter');
const adminContactRouter=require('./app/router/admin-contactRouter');

//Khoi tao app express
const app= new express();

//Khai bao cong chay
const port=4000;

//Cau hinh su dung json
app.use(express.json());
app.use(bodyParser.urlencoded({extended: false}));

mongoose.connect("mongodb://127.0.0.1:27017/mid_example")
	.then(() => {
		console.log('Successfully connected');
})
	.catch((err) => {
		console.log(err.message);
})


//Khai bao cac api
app.use("/",vaccinationRouter);
app.use("/admin/users/",adminUserRouter);
app.use("/admin/contacts/",adminContactRouter);
app.use("/users/",userRouter);
app.use("/contacts",contactRouter);
//start app

app.listen(port, ()=>{
    console.log(`app listening on port ${port} `);
})
const userModel = require('../models/userModel');
const mongoose = require('mongoose');
const path = require('path');

const createUser = async (req, res) => {
    //B1: thu thap du lieu
    const {
        reqName,
        reqPhone,
        reqStatus
    } = req.body
    //B2: validate du lieu
    if (!reqName) {
        res.status(400).json({
            message: "full name khong hop le"
        })
        return false;
    }
    else if (!reqPhone || Number.isNaN(reqPhone) || reqPhone.length < 10) {
        res.status(400).json({
            message: "Phone khong hop le",
        })
        return false;
    }
    else {
        try {
            //B3:Xu ly du lieu
            var newUser = {
                fullName: reqName,
                phone: reqPhone,
                status: reqStatus
            }
            console.log(newUser);
            const result = await userModel.create(newUser);
            return res.status(201).json({
                message:"Tao thanh cong",
                data:result
            })

        }
        catch (error) {
            console.log(error);
            return error.status(500).json({
                message: "Co loi xay ra"
            })
        }
    }
}
const getAllUser = async (req, res) => {
    //B1:Thu thap du lieu
    //B2:Validate du lieu
    //B3:Xu ly du lieu
    try {
        const result = await userModel.find();
        return res.status(200).json({
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getUserById = async (req, res) => {
    //B1:Thu thap du lieu
    const userid = req.params.userid;

    //B2:Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(userid)) {
        return res.status(400).json({
            message: "User Id khong hop le"
        })
    }

    //B3:Xu ly du lieu
    const result = await userModel.findById(userid);
    try {

        return res.status(200).json({
            message: "Lay danh sach user thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const updateUserById = async (req, res) => {
    //B1:Thu thap du lieu
    const userid = req.params.userid;
    const
        { reqFullName,
            reqPhone,
            reqStatus
        } = req.body

    //B2: validate du lieu
    if (reqFullName == "") {
        res.status(400).json({
            message: "Full name khong hop le"
        })
        return false;
    }
    else if (reqPhone == "") {
        res.status(400).json({
            message: "Phone khong hop le"
        })
        return false;
    }
    else {
        //B3: Xu ly du lieu
        try {
            var newUpdateUser = {};
            if (reqFullName) {
                newUpdateUser.fullName = reqFullName;
            }
            if (reqPhone) {
                newUpdateUser.phone = reqPhone;
            }
            if (reqStatus) {
                newUpdateUser.status = reqStatus;
            }

            var result = await userModel.findByIdAndUpdate(userid, newUpdateUser);
            if (result) {
                return res.status(200).json({
                    message: "Cap nhat du lieu thanh cong",
                    data: result
                })
            }
            else {
                return res.status(404).json({
                    message: "Khong tim thay thong tin user",
                })
            }

        } catch (error) {
            console.log(error);
            return res.status(500).json({
                message: "Co loi xay ra"
            })
        }
    }
}

const deleteUserById = async (req, res) => {
    //B1:Thu thap du lieu
    const userid = req.params.userid;
    //B2:
    if (!mongoose.Types.ObjectId.isValid(userid)) {
        return res.status(400).json({
            message: "User Id khong hop le"
        })
    }
    try {
        const result = await userModel.findByIdAndDelete(userid);

        if (result) {
            return res.status(200).json({
                message: "Xoa du lieu thanh cong",
            })
        }
        else {
            return res.status(404).json({
                message: "Khong tim thay thong tin user",
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}
module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById,
}
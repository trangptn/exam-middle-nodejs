const contactModel = require('../models/contactModel');
const mongoose = require('mongoose');
const path=require('path');

const createContact = async (req, res) => {
    //B1: thu thap du lieu
    const {
        reqEmail,
    } = req.body
    //B2: validate du lieu
    var emailReg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    if(!reqEmail || !emailReg.test(reqEmail)){
        res.status(400).json({
            message:"Email khong hop le"
        })
        return false;
    }
    else {
        try {
            //B3:Xu ly du lieu
            var newContact = {
                email: reqEmail,
            }

            const result = await contactModel.create(newContact);
            
            return res.redirect("/admin/contacts");
           
        }
        catch (error) {
            console.log(error);
            return error.status(500).json({
                message: "Co loi xay ra"
            })
        }
    }
}
const getAllContact = async (req, res) => {
    //B1:Thu thap du lieu
    //B2:Validate du lieu
    //B3:Xu ly du lieu
    try {
        const result = await contactModel.find();
        return res.status(200).json({
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getContactById = async (req, res) => {
    //B1:Thu thap du lieu
    const contactid = req.params.contactid;
    //B2:Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(contactid)) {
        return res.status(400).json({
            message: "Contact Id khong hop le"
        })
    }

    //B3:Xu ly du lieu
    const result = await contactModel.findById(contactid);
    try {

        return res.status(200).json({
            message: "Lay danh sach Contact thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const updateContactById = async (req, res) => {
    //B1:Thu thap du lieu
    const contactid=req.params.contactid;
    const reqEmail=req.params.reqEmail;

    //B2: validate du lieu
    var emailReg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    if(!reqEmail || !emailReg.test(reqEmail)){
        res.status(400).json({
            message:"Email khong hop le"
        })
        return false;
    }
    else{
    //B3: Xu ly du lieu
    try {
        var newUpdateContact= {};
        if (reqEmail) {
            newUpdateContact.email = reqEmail;
        }
        var result = await contactModel.findByIdAndUpdate(contactid, newUpdateContact);
        if (result) {
            return res.status(200).json({
                message: "Cap nhat du lieu thanh cong",
                data: result
            })
        }
        else {
            return res.status(404).json({
                message: "Khong tim thay thong tin contact",
            })
        }

    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}
}

const deleteContactById = async (req, res) => {
    //B1:Thu thap du lieu
    const contactid = req.params.contactid;
    //B2:
    if (!mongoose.Types.ObjectId.isValid(contactid)) {
        return res.status(400).json({
            message: "Contact Id khong hop le"
        })
    }
    try {
        const result = await contactModel.findByIdAndDelete(contactid);

        if (result) {
            return res.status(200).json({
                message: "Xoa du lieu thanh cong",
            })
        }
        else {
            return res.status(404).json({
                message: "Khong tim thay thong tin contact",
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}
module.exports = {
    createContact,
    getAllContact,
    getContactById,
    updateContactById,
    deleteContactById,
}
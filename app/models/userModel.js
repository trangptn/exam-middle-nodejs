const mongoose=require('mongoose');

const schema= mongoose.Schema;

const userSchema= new schema({
    fullName:{
        type:String,
        require: true
    },
    phone:{
        type:String,
        require:true,
        unique:true
    },
    status:{
        type:String,
        default:"Level 0"
    }
},
{
    timestamps:true
})

module.exports=mongoose.model("user",userSchema);
const mogoose=require('mongoose');

const schema= mogoose.Schema;

const contactSchema= new schema(
    {
        email:{
            type:String,
            require:true
        }
    }
)
module.exports=mogoose.model("contact",contactSchema);
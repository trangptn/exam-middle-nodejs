const getDateMiddleware=(req,res,next) =>{
    console.log("Thời gian hiện tại", new Date());
    next()
}

const getURLMiddleware=(req,res,next) =>{
    console.log("URL hiện tại",__dirname);
    next()
}

module.exports={
    getDateMiddleware,
    getURLMiddleware
}
const express=require('express');

const router=express.Router();

const path=require('path');

const {
    getDateMiddleware,
    getURLMiddleware
}=require('../middlewares/vaccination.Middleware');

router.get("/",getDateMiddleware,getURLMiddleware,(req,res)=>{
    res.sendFile(path.join(__dirname+'/../views/index.html'));});

router.use(express.static(__dirname + '/../views'));

module.exports=router;


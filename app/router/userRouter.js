const express=require('express');

const router=express.Router();

const{
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById,
}=require('../controllers/userController');

router.get("/",getAllUser);
router.get("/:userid",getUserById);
router.post("/",createUser);
router.put("/update/:userid",updateUserById);
router.delete("/:userid",deleteUserById);
module.exports=router;
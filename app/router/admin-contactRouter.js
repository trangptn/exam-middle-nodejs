const express=require('express');

const router=express.Router();

const path=require('path');

const{
    createContact,
    getAllContact,
    getContactById,
    updateContactById,
    deleteContactById,
}=require('../controllers/adminContactController');
router.get("/",(req,res)=>{
    res.sendFile(path.join(__dirname+'/../views/admin-contact.html'));
});

router.get("/getAll",getAllContact);
router.get("/:contactid",getContactById);
router.post("/",createContact);
router.put("/update/:contactid/:reqEmail",updateContactById);
router.delete("/:contactid",deleteContactById);

module.exports=router;


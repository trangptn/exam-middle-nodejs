const express=require('express');
const router=express.Router();

const{
    createContact,
    getAllContact,
    getConatctById,
    updateContactById,
    deleteContactById
}=require('../controllers/contactController');

router.post("/",createContact);
router.get("/",getAllContact);
router.get("/:contactid",getConatctById);
router.put("/:contactid",updateContactById);
router.delete("/:contactid",deleteContactById);

module.exports=router;
const express=require('express');

const router=express.Router();

const path=require('path');

const{
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById,
}=require('../controllers/adminUserController');
router.get("/",(req,res)=>{
    res.sendFile(path.join(__dirname+'/../views/admin.html'));
});

router.get("/getAll",getAllUser);
router.get("/:userid",getUserById);
router.post("/",createUser);
router.put("/update/:userid/:reqFullName/:reqPhone/:reqStatus",updateUserById);
router.delete("/:userid",deleteUserById);

module.exports=router;

